#!/usr/bin/env bash

# TIMEPOOL RANDOM GENERATOR
# Use subject to the CLEVER LICENSE included herewith.
# 3883@sugar.bug | https://sybershock.com | @firefly@neon.nightbulb.net (fedi)

# A demo of how to gather clock entropy from a deterministic environment.
# This demo is designed for visual aid with fluffy overkill.

license="
    TIMEPOOL RANDOM GENERATOR
    Use subject to the CLEVER LICENSE included herewith.
    
    This program generates strong seeds for feeding to a CSPRNG.

    command line options: [demo][hex][alpha][b32][b64][bin][help]

    AUTHOR and LICENSE and DISCLAIMER

    3883@sugar.bug | https://sybershock.com | @firefly@neon.nightbulb.net

                            WHATEVER IS CLEVER
                              PUBLIC LICENSE

                             <3883@sugar.bug>

           Do whatever is clever. Do as you wish with this product.
             Do whatever is clever shall be the whole of the law.

             By using this software you agree that the author shall
           not be held liable for any harm whatsoever. You also agree
            there is no warranty or guarantee of any kind whatsoever."

# run the script with one of these flags:
# [demo][hex][alpha][b32][b64][bin][help]
# example $> timepool hex ... $> timepool alpha ... $> timepool demo

# run with flag 'demo' to see output of internal transformations.
# example: $> timepool demo

# One can extrapolate more entropy by timing dice rolls than from dice face values. This overcomes the problem of a constrained system with an anemic or unknown random seed source. This is a digital equivalent of timing a dice roll then using system intransigences and proof of work hashing to stretch out the timings.

# Generate random seed on a system without well-seeded random number generator.
# Gather output from several iterations of this program and seed it to CSPRNG.
# Herein we hash, truncate, and whiten the timestamps for strong randomness.

flag="$@"

# Correct flags that have obvious intent.
[[ "$flag" == "" ]] && flag="help"
[[ "$flag" == "base32" ]] && flag="b32"
[[ "$flag" == "base64" ]] && flag="b64"
[[ "$flag" == "abc" ]] && flag="alpha"

# Check for valid command option flag.

[[ "$flag" != "demo" ]] && \
[[ "$flag" != "hex" ]] && \
[[ "$flag" != "alpha" ]] && \
[[ "$flag" != "b32" ]] && \
[[ "$flag" != "b64" ]] && \
[[ "$flag" != "bin" ]] && \
[[ "$flag" != "help" ]] && \
echo "timepool : error : bad option: try [demo][hex][alpha][b32][b64][bin][help]" >&2 && exit 1

[[ "$flag" == "help" ]] && echo "$license" && exit 0

pad=''
pool=''

modstamp() {
stamp="$(date +%N)"
stamp="$((10#$stamp%67108864))"
printf '%08d' "$stamp" | head -c 7 | tail -c 6
}

# Get a string of nanosecond reads whitened by modulus and truncating digits.
# This will be fed to the proof of work initialization for hashing.

for timestamps in {1..32}
do
nonce="$(modstamp)$nonce"
done
[[ "$flag" == "demo" ]] && echo -n "@demo : timestamps : " && echo -n "$nonce" | fold -w 6 | tr '\n' ' ' && echo ''
nonce="$(echo $nonce | tr -dc 0-9)"

# Perform proof of work to get a randomized clock initialization moment.

match="false"
while [[ "$match" == "false" ]]
do
nonce="$(echo -n "$nonce" | b2sum | tr -dc a-f0-9 | tail -c 66)"
[[ "$flag" == "demo" ]] && echo "@demo : init : $nonce"
[[ "${nonce:0:1}" == "0" ]] && match="true"
done
nonce="$(echo "$nonce" | cut -c 3-)"

# Hashing rounds to grow and truncate the pad data.

for rounds in {1..64}
do
nonce="$(modstamp) $(modstamp) $(modstamp) $nonce $(modstamp) $(modstamp) $(modstamp)"
[[ "$flag" == "demo" ]] && echo "@demo : rounds : $nonce"
nonce="$(echo $nonce | tr -dc a-f0-9)"
nonce="$(echo -n "$nonce$pad" | b2sum | tr -dc a-f0-9 | tail -c 96)" # discard a few leading bytes
pad="${nonce:0:71}$pad" # discard some tail portion of the hash
pad="$(echo -n $pad | head -c 1033)"
nonce="$(echo $nonce | head -c 64)"
pool="$(echo -n "$pad$nonce" | b2sum | tail -c 113 | tr -dc a-f0-9)$pool"
# [[ "$flag" == "demo" ]] && padtail="$(echo "$pad" | tail -c 64)" && echo "@demo : hashpad : $padtail"
done

# If output is hex or demo use this.
# Final whitening with proof of work hashing.

if [[ "$flag" == "demo" ]] || [[ "$flag" == "hex" ]] then

match="false"
while [[ "$match" == "false" ]]
do
nonce="$(echo -n "$nonce" | b2sum | tr -dc a-f0-9 | tail -c 66)"
[[ "${nonce:0:1}" == "0" ]] && match="true"
[[ "$flag" == "demo" ]] && echo "@demo : whitening : $nonce"
done # while
fi # if

# Truncate the pool and discard 2/3 of the data keeping every 3rd nybble

pool="$(echo $pool | fold -w 3 | while read line ; do echo -n "${line:0:1}" ; done)"

# Trigger banner for 'demo' flag

[[ "$flag" == "demo" ]] && echo && echo "@demo : FINAL OUTPUTS FOR DEMO MODE"

# Hex random output from the nonce value

[[ "$flag" == "demo" ]] && echo -n "@demo : hexadecimal  : " && echo "$nonce" | cut -c 3-

[[ "$flag" == "hex" ]] && echo "$nonce" | cut -c 3-

# Alphanumeric (passhphrase) random output from the pool

[[ "$flag" == "demo" ]] && echo -n "@demo : alphanumeric : " && \
echo "$pool" | xxd -p -r | tr -dc A-Za-z0-9 | fold -w 2 | \
while read line ; do echo -n "${line:0:1}" ; done | head -c 64 && echo ''

[[ "$flag" == "alpha" ]] && \
echo "$pool" | xxd -p -r | tr -dc A-Za-z0-9 | fold -w 2 | \
while read line ; do echo -n "${line:0:1}" ; done | head -c 64 && echo ''

# Base32 random output

[[ "$flag" == "demo" ]] && echo -n "@demo : base32       : " && \
echo "$pool" | fold -w 3  | \
while read line ; do echo -n "${line:0:1}" ; done | xxd -p -r | base32 | head -c 64 && echo ''

[[ "$flag" == "b32" ]] && \
echo "$pool" | fold -w 3  | \
while read line ; do echo -n "${line:0:1}" ; done | xxd -p -r | base32 | head -c 64 && echo ''

# Base64 random output

[[ "$flag" == "demo" ]] && echo -n "@demo : base64       : " && \
echo "$pool" | fold -w 5 | \
while read line ; do echo -n "${line:0:1}" ; done | head -c 128 | xxd -p -r | base64 | head -c 64 && echo ''

[[ "$flag" == "b64" ]] && \
echo "$pool" | fold -w 5 | \
while read line ; do echo -n "${line:0:1}" ; done | head -c 128 | xxd -p -r | base64 | head -c 64 && echo ''

# Binary random output

[[ "$flag" == "bin" ]] && \
echo "$pool" | fold -w 7 | while read line ; do echo -n "${line:0:1}" ; done | xxd -p -r | head -c 32

