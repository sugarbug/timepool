# TIMEPOOL RANDOM GENERATOR

### Use subject to the CLEVER LICENSE included herewith.
### 3883@sugar.bug | https://sybershock.com | @firefly@neon.nightbulb.net (fedi)

### A demo of how to gather clock entropy from a deterministic environment.
### This demo is designed for visual aid with fluffy overkill.

### This program generates strong seeds for feeding to a CSPRNG.

    command line options: [demo][hex][alpha][b32][b64][bin][help]

Run the script with one of these flags:
```
[demo][hex][alpha][b32][b64][bin][help]
example $> timepool hex ... $> timepool alpha ... $> timepool demo
```

Run with flag 'demo' to see output of internal transformations.
```
example: $> timepool demo
```

One can extrapolate more entropy by timing dice rolls than from dice face values. This overcomes the problem of a constrained system with an anemic or unknown random seed source. This is a digital equivalent of timing a dice roll then using system intransigences and proof of work hashing to stretch out the timings.

Generate random seed on a system without well-seeded random number generator.
Gather output from several iterations of this program and seed it to CSPRNG.
Herein we hash, truncate, and whiten the timestamps for strong randomness.

